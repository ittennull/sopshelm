package main

import (
	"github.com/dchest/uniuri"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func generateFileName() string {
	return filepath.Join(os.TempDir(), uniuri.New()+".yaml")
}

func getFilesWithSuffix(where, suffix string) (files []string) {
	err := filepath.Walk(where, func(path string, info os.FileInfo, err error) error {
		if err == nil && !info.IsDir() && strings.HasSuffix(path, suffix) {
			fullPath, err := filepath.Abs(path)
			if err != nil {
				log.Fatal(err)
			}
			files = append(files, fullPath)
		}
		return nil
	})
	if err != nil {
		log.Fatalf("Can't list files in %s directory: %s", where, err)
	}

	return files
}

func moveFile(sourcePath, destPath string) {
	inputFile, err := os.Open(sourcePath)
	if err != nil {
		log.Fatalf("Couldn't open source file %s: %s", sourcePath, err)
	}

	outputFile, err := os.Create(destPath)
	if err != nil {
		inputFile.Close()
		log.Fatalf("Couldn't open dest file %s: %s", destPath, err)
	}
	defer outputFile.Close()

	_, err = io.Copy(outputFile, inputFile)
	inputFile.Close()
	if err != nil {
		log.Fatalf("Writing to output file failed: %s", err)
	}

	// The copy was successful, so now delete the original file
	err = os.Remove(sourcePath)
	if err != nil {
		log.Fatalf("Failed removing original file %s: %s", sourcePath, err)
	}
}

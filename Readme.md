# sopshelm
`sopshelm` is a tool designed to work with [Argocd](https://argoproj.github.io/argo-cd/), [SOPS](https://github.com/mozilla/sops) and [Helm](https://helm.sh/). `sopshelm` wraps `helm` command and decrypts files that were encrypted by SOPS.

## Purpose
The goal is to save all application secrets in Git in encrypted form. That brings a challenge for Argocd because it doesn't know how to decrypt files. Argocd supports several application types, `sopshelm` only works with `helm` as you can guess from the name. `sopshelm` is added to Argocd image and used to decrypt secret files right before passing them to `helm`. `helm` command receives unencrypted files and proceeds as usual. Once `helm` command finishes `sopshelm` deletes decrypted files. Overall the process looks like this:

![diagram](docs/secrets-argocd.svg)

Benefits of this approach:
- secrets are kept together with the rest of the code and they are safe to publish to a public repository
- no need to install to your cluster custom kubernetes operators to manage secrets or fetch secrets from an external vault. The fewer moving parts the more stable a cluster and your application are

## Installation
1) There are a [couple of ways](https://argoproj.github.io/argo-cd/operator-manual/custom_tools) how to add custom tooling to Argocd. This example shows how to do it by defining a custom Argocd image:

    ```dockerfile
    FROM golang
    RUN GO111MODULE=on go get gitlab.com/ittennull/sopshelm@v1.2.2

    FROM argoproj/argocd:latest
    COPY --from=0 /go/bin /usr/local/bin/
    USER root  
    RUN cd /usr/local/bin && \
        mv helm helm.bin && \
        mv helm2 helm2.bin && \
        mv sopshelm helm && \
        ln helm helm2
    ENV HELM_BINARY_PATH=/usr/local/bin/helm.bin
    USER argocd
    ```

    This Dockerfile builds `sopshelm` tool, then it copies the tool to _argoproj/argocd_ image, renames existing `helm` binaries and replaces them with `sopshelm`. The environment variable _HELM_BINARY_PATH_ is used by `sopshelm` to run the original `helm` after decrypting the secrets.

2) Now we need to use this new image and also provide a few environment variables for SOPS to be able to access KMS. For example if Azure KeyVault is used SOPS will try to authenticate using environment variables _AZURE_TENANT_ID, AZURE_CLIENT_ID, AZURE_CLIENT_SECRET_. For other KMS types check [documentation](https://github.com/mozilla/sops). 
     
    If you follow [getting started](https://argoproj.github.io/argo-cd/getting_started/#1-install-argo-cd) of Argocd you get a big _install.yaml_ file with Argocd. You can extend this file to provide the additional environment variables. You need to do it for _argocd-repo-server_:
    ```yaml
    image: your-custom-image
    imagePullPolicy: Always
    name: argocd-repo-server
    env:
    - name: AZURE_TENANT_ID
      value: "xyz.onmicrosoft.com"
    - name: AZURE_CLIENT_ID
      value: "1166c781-60e0-4642-9948-87c8abdfa5ad"
    - name: AZURE_CLIENT_SECRET
      value: "secret value"
    ```

### Examples
Check out these examples to understand how to define secrets in your app:

- [app1](examples/app1) keeps all secrets in a separate _secrets.yaml_ file
- [app2](examples/app2) keeps secrets in kubernetes manifest itself (_templates/secret.yaml_)
- [app-of-apps](examples/app-of-apps) deploys app1 and app2
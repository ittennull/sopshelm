# Encrypt kubernetes manifest
This example shows how to define an app in Argocd using Helm.

This app has a standard Helm structure. The file _templates/secret.yaml_ is encrypted using `sops`. The difference from the [app1](../app1) example is that there is no separate _secrets.yaml_ file that contains only secrets. Instead the kubernetes secret manifest file is encrypted itself. As a result you don't need to provide additional `--values` parameters for Helm.

The example app contains a cronjob that prints all environment variables every minute - you can see that even though the data in _secret.yaml_ is encrypted in Git Argocd can decrypt them and continue working as if everything was in plain text.

## Deploy
To deploy the app using Argocd CLI run the command
```bash
argocd app create app2 --repo "https://gitlab.com/ittennull/sopshelm.git" --path examples/app2 #... rest of arguments
```

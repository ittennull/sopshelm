module gitlab.com/ittennull/sopshelm

go 1.15

require (
	github.com/dchest/uniuri v0.0.0-20200228104902-7aecb25e1fe5
	go.mozilla.org/sops/v3 v3.6.1
)
